#!../../bin/linux-x86_64/sht

## You may have to change sht to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/sht.dbd"
sht_registerRecordDeviceDriver pdbbase

## Load record instances
#dbLoadRecords("db/xxx.db","user=epics")

cd "${TOP}/iocBoot/${IOC}"

dbLoadRecords("sht.db","BL=PINK,DEV=SHUTTER")

iocInit

## Start any sequence programs
#seq sncxxx,"user=epics"
