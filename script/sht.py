#!/usr/bin/python3

import epics
import time

def isclose(pv):
    state = int(pv.value) & 0x1
    return state>0

def isopen(pv):
    state = int(pv.value) & 0x2
    return state>0

def isenable(pv):
    state = int(pv.value) & 0x4
    return state>0

fname="/EPICS/autosave/shutter.log"

## create PV channels
enable_pv = epics.PV("PINK:SHUTTER:enable", auto_monitor=True)
shtr_state = epics.PV("PSHY01U012L:State1", auto_monitor=True)
shtr_toggle = epics.PV("PSHY01U012L:SetTa", auto_monitor=False)
time.sleep(2)

## Info
msg = "[{}] Watching photon shutter state...\n".format(time.asctime())
with open(fname, "a") as ff:
    ff.write(msg)
print(msg)

## main loop
while(True):
    if enable_pv.value and isclose(shtr_state) and isenable(shtr_state):
        msg=[]
        msgstr="[{}] Shutter closed detected".format(time.asctime())
        print(msgstr)
        msg.append(msgstr+'\n')
        time.sleep(2)
        msgstr="[{}] Toggling shutter...".format(time.asctime())
        shtr_toggle.put(1)
        time.sleep(5)
        if isopen(shtr_state):
            msgstr="[{}] Shutter was automatically opened".format(time.asctime())
            print(msgstr)
            msg.append(msgstr+'\n')
        else:
            msgstr="[{}] Shutter failed to open".format(time.asctime())
            print(msgstr)
            msg.append(msgstr+'\n')
        with open(fname, "a") as ff:
            for mline in msg:
                ff.write(mline)
    time.sleep(5)

print("ok")
