# pink-autoshutter

Small IOC/python script to keep the BL shutter opened. 

## docker-composer.yml
```yml
version: "3.7"
services:
  ioc:
    image: docker.gitlab.gwdg.de/pink-bl/pink-autoshutter/autoshutter:v1.0
    container_name: autoshutter-ioc
    network_mode: host
    stdin_open: true
    tty: true
    restart: always
    working_dir: "/EPICS/IOCs/pink-autoshutter/iocBoot/iocsht"
    command: "./sht.cmd"
  py:
    image: docker.gitlab.gwdg.de/pink-bl/pink-autoshutter/autoshutter:v1.0
    container_name: autoshutter-py
    network_mode: host
    stdin_open: true
    tty: true
    restart: always
    depends_on:
        - "ioc"
    working_dir: "/EPICS/IOCs/pink-autoshutter/script"
    command: "./sht.py"
    volumes:
        - type: bind
          source: /EPICS/autosave/autoshutter
          target: /EPICS/autosave
```
